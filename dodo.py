# SPDX-License-Identifier: AGPL-3.0-or-later OR GPL-2.0-or-later OR CERN-OHL-S-2.0+ OR Apache-2.0
from pathlib import Path


### config


DOIT_CONFIG = {
    "default_tasks": ["docs", "unittest"],
}


### config


DOIT_CONFIG = {
    "default_tasks": ["unittest"],
}


### globals


top_dir = Path(__file__).parent
klayout_dir = top_dir.joinpath("pdkmaster", "io", "klayout")
test_dir = top_dir.joinpath("test")

klayout_deps = klayout_dir.rglob("*.py")
test_deps = test_dir.rglob("*.py")

### main tasks


#
# test
test_report_file = test_dir.joinpath("cover_report.log")
def task_unittest():
    """Run unittests with and coverage"""

    def check_100pct():
        with test_report_file.open() as f:
            for line in f:
                if line.startswith("TOTAL"):
                    assert line.split()[-1] == "100%", "Code not fully covered"
                    break
            else:
                raise Exception("TOTAL not found in report file")

    return {
        "title": lambda _: "Running unittests and coverage",
        "file_dep": (*klayout_deps, *test_deps),
        "targets": (test_report_file,),
        "actions": (
            f"coverage run --include 'pdkmaster/*' -m unittest discover -s test -p '*.py' 2>&1",
            f"coverage report -m 2>&1 | tee {test_report_file}",
            check_100pct,
        )
    }
