# SPDX-License-Identifier: AGPL-3.0-or-later OR GPL-2.0-or-later OR CERN-OHL-S-2.0+ OR Apache-2.0
from pathlib import Path
import unittest

from pdkmaster.task import OpenPDKTree
from pdkmaster.io import klayout as _kl

from ...dummy import (
    dummy_tech, dummy_lib, dummy_gdslayers, dummy_prims_spiceparams,
)


class DoitTest(unittest.TestCase):
    def test_taskmanager(self):
        top_dir = Path("/tmp")
        openpdk_tree = OpenPDKTree(
            top=top_dir.joinpath("open_pdk"), pdk_name=dummy_tech.name,
        )

        tm = _kl.TaskManager(
            tech_cb=(lambda: dummy_tech),
            lib4name_cb=(lambda _: dummy_lib),
            cell_list={"Dummy": ["cell1", "Gallery"]},
            top_dir=top_dir, openpdk_tree=openpdk_tree,
            gdslayers_cb=(lambda: dummy_gdslayers),
        )

        # We generate data for each task and execute the task
        # correction is assumed to be done in lower level unit tests

        export_t = tm.create_export_task(
            spice_params_cb=(lambda: dummy_prims_spiceparams),
        )
        _ = export_t.task_func()
        export_t._gen_klayout()

        gds_t = tm.create_gds_task()
        _ = tuple(gds_t.task_func_gds())
        gds_t._gen_gds("Dummy")

        drc_t = tm.create_drc_task()
        _ = tuple(drc_t.task_func_drc())
        _ = tuple(drc_t.task_func_drccells())
        gds_dir = openpdk_tree.views_dir(lib_name="Dummy", view_name="gds")
        gds_file = gds_dir.joinpath("Dummy.gds")
        rep_file = tm.out_dir_drc.joinpath("cell1.rep")
        drc_t._run_drc("Dummy", "cell1", gds_file, rep_file)
        # TODO: add run with a DRC error to check that code path

        lvs_t = tm.create_lvs_task()
        _ = tuple(lvs_t.task_func_lvs())
        _ = tuple(lvs_t.task_func_lvscells())
        gds_file = gds_dir.joinpath("Dummy.gds")
        spice_file = openpdk_tree.top.joinpath("unavailable", "Dummy_lvs.spi")
        rep_file = tm.out_dir_lvs.joinpath("cell1.lvsdb")
        with self.assertRaises(_kl.CheckError):
            lvs_t._run_lvs(
                "Dummy", "cell1", gdsfile=gds_file, spicefile=spice_file, lvsdb=rep_file,
            )

        _ = tm.task_func_signoff()

