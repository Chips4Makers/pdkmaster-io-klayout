# SPDX-License-Identifier: AGPL-3.0-or-later OR GPL-2.0-or-later OR CERN-OHL-S-2.0+ OR Apache-2.0
import unittest

from pdkmaster.technology import geometry as _geo
from pdkmaster.io.klayout import geometry as _klgeo


class GeometryTest(unittest.TestCase):
    def test_text(self):
        self.assertEqual(
            _klgeo.text(".", mag=10, grid=0.1),
            _geo.Rect(left=2.0, bottom=0.0, right=3.0, top=1.0),
        )
