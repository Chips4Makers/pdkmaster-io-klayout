# SPDX-License-Identifier: AGPL-3.0-or-later OR GPL-2.0-or-later OR CERN-OHL-S-2.0+ OR Apache-2.0
import unittest
from typing import cast

from pdkmaster.technology import geometry as _geo
from pdkmaster.io.klayout import import_ as _imp, export as _exp


class ImportExportTest(unittest.TestCase):
    def test_importexport(self):
        import pya

        rect = _geo.Rect.from_floats(values=(0.0, 0.0, 1.0, 1.0))
        polygon = _geo.Polygon.from_floats(points=(
            (0.0, 0.0), (0.0, 1.0), (1.0, 1.0), (1.0, 2.0),
            (2.0, 2.0), (2.0, 0.0), (0.0, 0.0),
        ))
        ms = _geo.MultiShape(shapes=(
            # Make polygons not overlapping
            rect, polygon.moved(dxy=_geo.Point(x=2.0, y=0.0)),
        ))
        eps = _geo.epsilon
        box = pya.Box(0, 0, round(1.0/eps), round(1.0/eps))
        region = pya.Region(box)
        region += box.moved(pya.Vector(round(2.0/eps), 0)) # type: ignore

        self.assertEqual(_imp.import_region2poly(_exp.export_poly2region(rect)), rect)
        self.assertEqual(_imp.import_poly2poly(cast(pya.Polygon, box)), rect)
        self.assertEqual(_imp.import_region2poly(_exp.export_poly2region(polygon)), polygon)

        with self.assertRaises(ValueError):
            # Empty region
            _imp.import_region2shape(pya.Region())
        with self.assertRaises(ValueError):
            # Multiple polygons
            _imp.import_region2poly(region)